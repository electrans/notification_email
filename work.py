# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.modules.electrans_notification_email.user_email import (
    CurrentUserMixin)


class Project(CurrentUserMixin, metaclass=PoolMeta):
    __name__ = 'work.project'