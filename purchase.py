from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.modules.electrans_notification_email.user_email import (
    CurrentUserMixin)


class PurchaseRequisition(metaclass=PoolMeta):
    __name__ = 'purchase.requisition'

    employee_email = fields.Function(
        fields.One2Many('res.user', 'user', 'Employee email'),
        'get_employee_email'
    )

    def get_employee_email(self, name):
        User = Pool().get('res.user')
        return [User.search([('employee', '=', self.employee)])[0]]


class Purchase(CurrentUserMixin, metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
