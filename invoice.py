from trytond.model import fields
from trytond.pool import PoolMeta


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    project_manager_email = fields.Function(
        fields.Char('Project Manager Email'),
        'get_project_manager_email')

    def get_project_manager_email(self, name):
        """
        Returns the project's manager email of the project linked to the sale
        """
        return (self.work_project.project_manager.party.email if
                self.work_project and self.work_project.project_manager else None)
