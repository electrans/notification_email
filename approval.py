from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool


class Group(metaclass=PoolMeta):
    'Approval Group'
    __name__ = 'approval.group'

    users_to_email = fields.One2Many('approval.group-res.user', 'group', 'Notifications')


class GroupUser(ModelView, metaclass=PoolMeta):
    'Approval Group - Users'
    __name__ = 'approval.group-res.user'

    allow_notif = fields.Boolean("Receive notifications")


class Request(metaclass=PoolMeta):
    'Approval Request'
    __name__ = 'approval.request'

    users_email = fields.Function(
        fields.One2Many('res.user', 'user', 'Users to receive notifications'),
        'get_users_to_notificate'
    )

    def get_users_to_notificate(self, name):
        RelGroup = Pool().get('approval.group-res.user')
        users_enabled = RelGroup.search([
            ('group', '=', self.group),
            ('allow_notif', '=', 'True')
        ])
        return [x.user.id for x in users_enabled]
