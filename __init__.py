# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool

from . import approval
from . import purchase
from . import material_request
from . import notification
from . import operation
from . import sale
from . import user_email
from . import stock
from . import work
from . import invoice


def register():
    Pool.register(
        approval.Group,
        approval.GroupUser,
        approval.Request,
        purchase.Purchase,
        purchase.PurchaseRequisition,
        material_request.MaterialRequest,
        notification.Email,
        operation.Operation,
        sale.Sale,
        stock.ShipmentInternal,
        stock.ShipmentOut,
        user_email.CurrentUserMixin,
        invoice.Invoice,
        work.Project,
        module='electrans_notification_email', type_='model')
