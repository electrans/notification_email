from trytond.pool import PoolMeta


class Email(metaclass=PoolMeta):
    __name__ = 'notification.email'

    @classmethod
    def __setup__(cls):
        super(Email, cls).__setup__()
        cls.fallback_recipients.states = {}

