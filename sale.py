from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.modules.electrans_notification_email.user_email import (
    CurrentUserMixin)


class Sale(CurrentUserMixin, metaclass=PoolMeta):
    __name__ = 'sale.sale'

    salesman_user = fields.Function(
        fields.Many2One('party.party', 'Salesman'),
        'get_salesman_user'
    )
    fill_terms_employees_email = fields.Function(
        fields.One2Many('res.user', None, 'Employee to fill Line terms',
                        help='Employees that can fill the line terms'),
        'get_fill_terms_employees'
    )
    quoted_by_group = fields.Function(
        fields.Many2One('party.party', 'Quoted By Group'),
        'get_quoted_by_group'
    )
    project_manager_email = fields.Function(
        fields.Char('Project Manager Email'),
        'get_project_manager_email')

    def get_salesman_user(self, name):
        return self.employee.party.id if self.employee else None

    def get_quoted_by_group(self, name):
        return self.quoted_by.party.id if self.quoted_by and self.quoted_by.party else None

    def get_fill_terms_employees(self, name):
        """
        Return the employees that can fill the line terms from sale.configuration
        """
        pool = Pool()
        Config = pool.get('sale.configuration')
        config = Config(1)
        if config.users_to_notify:
            return [user.id for user in config.users_to_notify]

    def get_project_manager_email(self, name):
        """
        Returns the project's manager email of the project linked to the sale
        """
        return (self.work_project.project_manager.party.email if
                self.work_project and self.work_project.project_manager else None)
