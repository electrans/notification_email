Notification Email - Electrans
###############

The current module extends approval and notification_email.

approval:
notification_email

Approval
********

Approval was modified to allow send notifications a selected 'party' (more than 1) and added template to approval
requests.


Notification_email
******************

Notification_email: added record to send email when "Approval request" is created or modified.
