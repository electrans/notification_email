from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.electrans_notification_email.user_email import (
    CurrentUserMixin)


class MaterialRequest(CurrentUserMixin, metaclass=PoolMeta):
    __name__ = 'material.request'

    project_manager_email = fields.Function(
        fields.Char('Project Manager Email'),
        'get_project_manager_email')

    def get_project_manager_email(self, name):
        """
        Returns the project's manager email of the project linked to the sale
        """
        return (self.project.project_manager.party.email if
                self.project and self.project.project_manager else None)
