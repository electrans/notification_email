from trytond.pool import PoolMeta
from trytond.model import fields


class Operation(metaclass=PoolMeta):
    __name__ = 'production.operation'

    production_done = fields.Function(fields.Boolean('Production Complete'),
                                      '_is_production_done')

    def _is_production_done(self, name):
        """
            Return True if all production operations done.
        """
        for operation in self.production.operations:
            if operation.state != 'done':
                return False
        return True
