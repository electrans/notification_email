# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.modules.electrans_notification_email.user_email import (
    CurrentUserMixin)


class ShipmentInternal(CurrentUserMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'


class ShipmentOut(CurrentUserMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    project_manager = fields.Function(
        fields.Many2One('company.employee', 'Project Manager'),
        'get_project_manager')

    def get_project_manager(self, name=None):
        if self.project and self.project.project_manager:
            return self.project.project_manager
        return
